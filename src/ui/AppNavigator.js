import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {
  AddNewRestaurant,
  EditRestaurant,
  Home,
  RestaurantDetail,
} from '../ui/screens';

const Stack = createStackNavigator();
const Navigator = Stack.Navigator;
const Screen = Stack.Screen;

export function AppNavigator() {
  return (
    <>
      <NavigationContainer>
        <Navigator>
          <Screen name="Restaurants" component={Home} />
          <Screen name="Edit Restaurant" component={EditRestaurant} />
          <Screen name="Add Restaurant" component={AddNewRestaurant} />
          <Screen name="Restaurant Details" component={RestaurantDetail} />
        </Navigator>
      </NavigationContainer>
    </>
  );
}
