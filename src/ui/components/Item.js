import React from 'react';
import {TouchableOpacity} from 'react-native';
import {Card, ListItem} from 'react-native-elements';

export const Item = ({id, name, onLongPress, onPress}) => {
  return (
    <>
      <Card>
        <ListItem
          title={name}
          Component={TouchableOpacity}
          onPress={onPress}
          onLongPress={() => {
            onLongPress(id, name);
          }}
        />
      </Card>
    </>
  );
};
