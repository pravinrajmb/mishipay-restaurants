import React, {useState} from 'react';
import {FlatList, SafeAreaView, StyleSheet, View} from 'react-native';
import {Button} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import Modal from 'react-native-modal';
import {useDispatch} from 'react-redux';

import {Item} from '../components';
import {restaurantActions} from '../../actions';

export const Home = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const restaurants = useSelector(state => state.restaurantReducer.restaurants);
  const [isModalVisible, showModal] = useState({data: {}, visible: false});
  navigation.setOptions({
    title: 'Restaurants',
    headerRight: () => (
      <Button
        style={styles.headerButton}
        title="Add"
        type="clear"
        onPress={() => {
          navigation.navigate('Add Restaurant');
        }}
      />
    ),
  });

  function closeModal() {
    showModal({data: {}, visible: false});
  }

  function openModal(id, name) {
    showModal({data: {id, name}, visible: true});
  }

  return (
    <>
      <SafeAreaView>
        <FlatList
          data={restaurants}
          renderItem={({item}) => (
            <Item
              id={item.id}
              name={item.name}
              onLongPress={(id, name) => {
                openModal(id, name);
              }}
              onPress={() => {
                navigation.navigate('Restaurant Details');
              }}
            />
          )}
          keyExtractor={item => item.id}
        />

        <Modal
          isVisible={isModalVisible.visible}
          onSwipeComplete={closeModal}
          isSwipeable={true}
          onBackdropPress={closeModal}
          onBackButtonPress={closeModal}
          swipeDirection={'down'}
          style={styles.bottomSheet}>
          <View style={styles.bottomSheetContent}>
            <Button
              title="Edit"
              containerStyle={styles.bottomSheetButton}
              onPress={() => {
                closeModal();
                navigation.navigate('Edit Restaurant', {
                  id: isModalVisible.data.id,
                  name: isModalVisible.data.name,
                });
              }}
            />
            <Button
              title="Delete"
              containerStyle={styles.bottomSheetButton}
              onPress={() => {
                dispatch(actionCreators.delete(isModalVisible.data.id));
                closeModal();
              }}
            />
          </View>
        </Modal>
      </SafeAreaView>
    </>
  );
};

const actionCreators = {
  delete: restaurantActions.deleteRestaurant,
};

const styles = StyleSheet.create({
  headerButton: {paddingEnd: 8},
  bottomSheet: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  bottomSheetContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  bottomSheetButton: {
    width: '100%',
    margin: 10,
  },
});
