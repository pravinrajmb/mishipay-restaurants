import React, {useState} from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Button, Card, Input} from 'react-native-elements';
import {useNavigation} from '@react-navigation/native';
import {useDispatch} from 'react-redux';

import {restaurantActions} from '../../actions';

export const AddNewRestaurant = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [restaurantName, setRestaurantName] = useState('');
  navigation.setOptions({
    title: 'Restaurants',
    headerRight: () => (
      <Button
        style={styles.headerButtonRight}
        title="Save"
        type="clear"
        onPress={() => {
          if (restaurantName) {
            dispatch(actionCreators.add(restaurantName));
            navigation.goBack();
          }
        }}
      />
    ),
  });
  return (
    <>
      <SafeAreaView>
        <Card>
          <Input
            placeholder="Oxford Restro Bar"
            label="Restaurant name"
            value={restaurantName}
            onChangeText={setRestaurantName}
          />
        </Card>
      </SafeAreaView>
    </>
  );
};

const actionCreators = {add: restaurantActions.addRestaurant};

const styles = StyleSheet.create({
  headerButtonRight: {paddingEnd: 8},
  headerButtonLeft: {paddingStart: 8},
});
