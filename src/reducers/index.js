import {combineReducers} from 'redux';
import {restaurantReducer} from './RestaurantReducer';

const rootReducer = combineReducers({
  restaurantReducer,
});

export default rootReducer;
