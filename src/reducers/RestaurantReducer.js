import {restaurantConstants} from '../constants';

let initialState = {restaurants: []};

export function restaurantReducer(state = initialState, action) {
  switch (action.type) {
    case restaurantConstants.ADD_RESTAUTANT:
      return {...state, restaurants: [...state.restaurants, action.payload]};
    case restaurantConstants.EDIT_RESTAURANT:
      return {
        ...state,
        restaurants: state.restaurants.map(restaurant =>
          restaurant.id === action.payload.id
            ? {...restaurant, name: action.payload.name}
            : restaurant,
        ),
      };
    case restaurantConstants.DELETE_RESTAURANT:
      return {
        ...state,
        restaurants: state.restaurants.filter(
          item => item.id !== action.payload.id,
        ),
      };
    default:
      return state;
  }
}
