import {restaurantConstants} from '../constants';
import uuid from 'react-native-uuid';

export const restaurantActions = {
  addRestaurant,
  deleteRestaurant,
  editRestaurant,
};

function addRestaurant(name) {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: restaurantConstants.ADD_RESTAUTANT,
        payload: {id: uuid(), name},
      });
      resolve();
    });
  };
}

function deleteRestaurant(id) {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({type: restaurantConstants.DELETE_RESTAURANT, payload: {id}});
      resolve();
    });
  };
}

function editRestaurant(id, name) {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch({
        type: restaurantConstants.EDIT_RESTAURANT,
        payload: {id: id, name: name},
      });
      resolve();
    });
  };
}
